GO ?= go

all: gdoh

gdoh:
	$(GO) build $(GOFLAGS) -o $@ .

clean:
	$(RM) gdoh

.PHONY: all gdoh clean
