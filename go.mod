module git.sr.ht/~rumpelsepp/gdoh

go 1.14

require (
	git.sr.ht/~rumpelsepp/rlog v0.0.0-20191119152513-6f7f3bf18e94
	git.sr.ht/~sircmpwn/getopt v0.0.0-20191230200459-23622cc906b3
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.7.4
	github.com/miekg/dns v1.1.31
	golang.org/x/net v0.0.0-20201002202402-0a1ea396d57c
)
